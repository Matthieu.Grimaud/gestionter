package tp1;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class GestionTER {
	public ArrayList<Groupe> groupes;
	public ArrayList<Sujet> sujets;
	
	public GestionTER() {
	  this.groupes = new ArrayList<Groupe>();
	  this.sujets = new ArrayList<Sujet>();
	}
	
	public void addGroupe(String nom) {
	  this.groupes.add(new Groupe(nom));
	}
	public void addSujet(String titre) {
	  this.sujets.add(new Sujet(titre));
	}
	public ArrayList<Sujet> getSujets() {
	  return this.sujets;
	}
	public int getNbGroupe() {
	  return this.groupes.size();
	}
	public Groupe getGroupe(int id) {
	  if(id >= 0 && id < this.getNbGroupe()) {
	    return this.groupes.get(id);
	  }
	  return new Groupe("n'existe pas");
	}
	
	public String toString() {
	  String str = "Liste groupe:";
	  for(int i=0; i<this.groupes.size(); i++) {
	    str += this.groupes.get(i).toString() + "\n";
	  }
	  str += "\n\nListe des sujets:";
	  for(int j=0; j<this.sujets.size(); j++) {
	    str += this.sujets.get(j).toString() + "\n";
	  }
	  return str;
	}
	
	
}