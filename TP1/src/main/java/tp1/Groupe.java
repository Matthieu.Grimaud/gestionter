package tp1;

import java.util.ArrayList;
import java.util.HashMap;

public class Groupe {
  public int id;
  public static int idP;
  public String nom;
  public HashMap<Integer, Sujet> voeux;
  public boolean voeuFini;
  private ArrayList<Sujet> listeSujet = new ArrayList<>();

  public Groupe() {}

  public Groupe(int id, int idPartage, String nom, HashMap<Integer, Sujet> voeux, boolean voeuFini) {
    this.id = id;
    Groupe.idP = idP;
    this.nom = nom;
    this.voeux = voeux;
    this.voeuFini = voeuFini;
  }

  public Groupe(String nom) {
    this.id = idP;
    idP = idP + 1;
    this.nom = nom;
    this.voeux = new HashMap<Integer, Sujet>();
    this.voeuFini = false;
  }

  public boolean getVoeuFini() {
	  return this.voeuFini;
  }
  
  public void setNom(String nom) {
    this.nom = nom;
  }
  public String getNom() {
    return this.nom;
  }
  public int getId() {
    return this.id;
  }
  public void setVoeuFini(boolean voeuFini) {
    this.voeuFini = voeuFini;
  }
  
  public void setVoeux(HashMap<Integer, Sujet> hash) {
    this.voeux = hash;
  }

  
  public String toString() {
    String str = this.nom + " (id:" + this.id + ")\n"; 
    for(Integer key : this.voeux.keySet()) {
      str += "--" + key + ": " + this.voeux.get(key).toString() + "\n";
    }
    return str;
  }
  
  public void ajouter(Sujet s) {
		listeSujet.add(s);
	}

}

/*
 private HashMap<String, NoticeBiblio> catalogue;

  public Catalogue() {
    catalogue = new HashMap<String, NoticeBiblio>();**/
